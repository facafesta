package facafesta;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

public abstract class Servlet extends HttpServlet {
	
	interface Conv<T> {
		T conv(String value) throws Exception;
	}
	
	private static final long serialVersionUID = 1L;
	private HttpServletRequest request;
	protected List<String> erros;
	
	protected void setRequest(HttpServletRequest request) {
		erros = new LinkedList<String>();
		this.request = request;
	}
	
	protected void addErro(String erro) {
		erros.add("<div class=\"msg\">" + erro + "</div>");
	}
	
	protected void addErro(List<String> erros) {
		for (String erro : erros) {
			addErro(erro);
		}
	}
	
	protected void mostrarErros() {
		request.setAttribute("erros", erros);
	}
	
	private <T> T getParam(String param, String regex, Conv<T> converter) {
		String value = request.getParameter(param);
		if (value == null || !value.matches(regex)) {
			erros.add("<div class=\"field\">" + param + "</div>");
			return null;
		}
		try {
			return converter.conv(value);
		} catch (Exception e) {
			return null;
		}
	}
	
	protected BigDecimal getMoney(String param) {
		return getParam(param, "\\d{1,3}(\\.\\d{1,3})*,\\d\\d", new Conv<BigDecimal>() {
			public BigDecimal conv(String value) {
				return new BigDecimal(value.replace(".", "").replace(',', '.'));
			}
		});
	}
	
	protected Integer getInteger(String param) {
		return getParam(param, "\\d+", new Conv<Integer>() {
			public Integer conv(String value) {
				return new Integer(value);
			}
		});
	}
	
	protected Long getLong(String param) {
		return getParam(param, "\\d+", new Conv<Long>() {
			public Long conv(String value) {
				return new Long(value);
			}
		});
	}
	
	protected Date getDate(String param) {
		return getParam(param, "\\d\\d/\\d\\d/\\d\\d\\d\\d", new Conv<Date>() {
			public Date conv(String value) throws ParseException {
				return new SimpleDateFormat("dd/MM/yyyy").parse(value);
			}
		});
	}
	
	protected String getEmail(String param) {
		return getParam(param, "([A-Za-z0-9-_%]\\.)*[A-Za-z0-9-_%]+@([A-Za-z0-9-_]+\\.)+[A-Za-z]{2,4}", new Conv<String>() {
			public String conv(String value) {
				return value;
			}
		});
	}
	
	protected String getString(String param) {
		String parameter = request.getParameter(param);
		if (parameter != null && parameter.isEmpty()) {
			return null;
		}
		return parameter;
	}
	
}
