package facafesta;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import junit.framework.Assert;

import org.junit.Test;

public class Util {

	private static EntityManagerFactory factory;

	private static InputStream getResource(String resource) {
		return Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
	}

	public static String getText(String resource) {
		InputStreamReader reader = new InputStreamReader(getResource(resource));
		StringBuilder s = new StringBuilder();
		char[] cbuf = new char[255];
		int read;
		try {
			while ((read = reader.read(cbuf)) != -1) {
				s.append(cbuf, 0, read);
			}
		} catch (IOException e) {
			System.err.println(e);
		}
		try {
			reader.close();
		} catch (IOException e) {
			System.err.println(e);
		}
		return s.toString();
	}

	@Test
	public void testGetText() {
		Assert.assertFalse(getText("META-INF/persistence.xml").equals(""));
	}

	public static String capitalize(String string) {
		if (string == null) {
			return null;
		} else if (string.length() <= 1) {
			return string.toUpperCase();
		} else {
			return Character.toUpperCase(string.charAt(0)) + string.substring(1);
		}
	}
	
	public static EntityManager getEntityManager() {
		return getEntityManagerFactory().createEntityManager();
	}
	
	private static EntityManagerFactory getEntityManagerFactory() {
		if (factory == null) {
			synchronized ("a") {
				if (factory == null) {
					factory = Persistence.createEntityManagerFactory("a");
				}
			}
		}
		return factory;
	}

}
