package facafesta.controller;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import facafesta.Servlet;
import facafesta.Util;

@WebServlet("/itens")
public class ListarItem extends Servlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		setRequest(req);
		EntityManager manager = Util.getEntityManager();
		Query query = manager.createQuery("from Item");
		String categoria = getString("categoria");
		if (categoria != null) {
			query = manager.createQuery("from Item where categoria = :cat");
			query.setParameter("cat", categoria);
		}
		Object itens = query.getResultList();
		req.setAttribute("itens", itens);
		getServletContext().getRequestDispatcher("/WEB-INF/item-list.jsp").forward(req, resp);
		manager.close();
	}
	
}
