package facafesta.controller;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import facafesta.Servlet;
import facafesta.Util;
import facafesta.model.Orcamento;
import facafesta.model.Reserva;

@WebServlet("/reserva")
public class CriarReserva extends Servlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		getServletContext().getRequestDispatcher("/WEB-INF/reserva-form.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		setRequest(req);
		EntityManager manager = Util.getEntityManager();
		manager.getTransaction().begin();
		Reserva reserva = new Reserva();
		reserva.setData(getDate("data"));
		reserva.setEntrega(getString("entrega"));
		reserva.setEvento(getString("evento"));
		reserva.setEndereco(getString("endereco"));
		reserva.setBairro(getString("bairro"));
		reserva.setNomeContato(getString("nome"));
		reserva.setEmailContato(getEmail("email"));
		reserva.setTelefoneContato(getString("telefone"));
		
		HttpSession session = req.getSession();
		Orcamento orcamento = (Orcamento) session.getAttribute("orcamento");
		reserva.setOrcamento(orcamento);
		
		addErro(orcamento.reservar());
		
		if (erros.isEmpty()) {
			manager.persist(reserva);
			manager.getTransaction().commit();
			session.setAttribute("orcamento", null);
			resp.sendRedirect("/reserva/"+reserva.getId().intValue());
		} else {
			mostrarErros();
			manager.getTransaction().rollback();
			session.setAttribute("reserva", reserva);
			doGet(req, resp);
		}
		manager.close();
	}
	
}
