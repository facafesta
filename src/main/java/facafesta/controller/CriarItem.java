package facafesta.controller;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import facafesta.Servlet;
import facafesta.Util;
import facafesta.model.Item;

@WebServlet("/item/novo")
public class CriarItem extends Servlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		getServletContext().getRequestDispatcher("/WEB-INF/item-form.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		setRequest(req);
		EntityManager manager = Util.getEntityManager();
		manager.getTransaction().begin();
		Item item = new Item();
		item.setCategoria(getString("categoria"));
		item.setDescricao(getString("descricao"));
		item.setValor(getMoney("valor"));
		item.setQuantidade(getInteger("quantidade"));
		if (erros.isEmpty()) {
			manager.persist(item);
			manager.getTransaction().commit();
			resp.sendRedirect("/item/"+item.getId().intValue());
		} else {
			mostrarErros();
			req.setAttribute("item", item);
			doGet(req, resp);
		}
		manager.close();
	}
	
}
