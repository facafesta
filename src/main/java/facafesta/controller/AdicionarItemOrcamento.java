package facafesta.controller;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import facafesta.Servlet;
import facafesta.Util;
import facafesta.model.Item;
import facafesta.model.ItemOrcamento;
import facafesta.model.Orcamento;

@WebServlet("/orcamento/adicionaritem")
public class AdicionarItemOrcamento extends Servlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		setRequest(req);
		Long idItem = new Long(getString("item"));
		Integer quantidade = getInteger("quantidade");
		HttpSession session = req.getSession(true);
		
		Orcamento orcamento = (Orcamento) session.getAttribute("orcamento");
		if (orcamento == null) {
			orcamento = new Orcamento();
		}
		
		EntityManager manager = Util.getEntityManager();
		
		Item item = (Item) manager.createQuery("from Item where id = :id").setParameter("id", idItem).getSingleResult();
		ItemOrcamento e = new ItemOrcamento();
		e.setItem(item);
		e.setQuantidade(quantidade);
		orcamento.getItens().add(e);
		
		session.setAttribute("orcamento", orcamento);
		manager.close();
		
		resp.sendRedirect("/orcamento");
	}
	
}
