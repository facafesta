package facafesta.controller;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import facafesta.Servlet;
import facafesta.Util;
import facafesta.model.Reserva;

@WebServlet("/reserva/*")
public class VerReserva extends Servlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		EntityManager manager = Util.getEntityManager();
		String[] parts = req.getRequestURI().substring(1).split("/");
		Long idReserva = new Long(parts[1]);
		Reserva reserva = (Reserva) manager.createQuery("from Reserva where id = :id").setParameter("id", idReserva).getSingleResult();
		req.setAttribute("reserva", reserva);
		getServletContext().getRequestDispatcher("/WEB-INF/reserva-show.jsp").forward(req, resp);
		manager.close();
	}
	
}
