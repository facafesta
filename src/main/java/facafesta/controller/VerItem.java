package facafesta.controller;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import facafesta.Servlet;
import facafesta.Util;
import facafesta.model.Item;

@WebServlet("/item/*")
public class VerItem extends Servlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		setRequest(req);
		String[] parts = req.getRequestURI().substring(1).split("/");
		if (parts.length != 2 || !parts[1].matches("\\d+")) {
			naoEncontrado(resp);
		} else {
			EntityManager manager = Util.getEntityManager();
			Long idItem = new Long(parts[1]);
			Item item = (Item) manager.createQuery("from Item where id = :id").setParameter("id", idItem).getSingleResult();
			if (item == null) {
				naoEncontrado(resp);
			} else {
				req.setAttribute("item", item);
				getServletContext().getRequestDispatcher("/WEB-INF/item-show.jsp").forward(req, resp);
			}
			manager.close();
		}
	}

	private void naoEncontrado(HttpServletResponse resp) throws IOException {
		addErro("Item n�o encontrado.");
		resp.sendRedirect("/itens");
	}
	
}
