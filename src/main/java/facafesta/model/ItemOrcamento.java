package facafesta.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

@Entity
public class ItemOrcamento {

	private Long id;
	private Item item;
	private Integer quantidade;

	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@OneToOne
	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	@Transient
	public BigDecimal getValorTotal() {
		return item.getValor().multiply(new BigDecimal(quantidade));
	}

	public String reservar() {
		if (item.getQuantidade() < quantidade) {
			return "A quantidade dispon�vel de " + item.getCategoria() + ":" + item.getDescricao() + " � insuficiente: " + item.getQuantidade() + ".";
		} else {
			item.setQuantidade(item.getQuantidade() - quantidade);
		}
		return null;
	}

}
