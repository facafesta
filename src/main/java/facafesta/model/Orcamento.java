package facafesta.model;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class Orcamento {

	private Long id;
	private List<ItemOrcamento> itens = new LinkedList<ItemOrcamento>();

	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@OneToMany(cascade=CascadeType.ALL)
	public List<ItemOrcamento> getItens() {
		return itens;
	}

	public void setItens(List<ItemOrcamento> itens) {
		this.itens = itens;
	}

	@Transient
	public BigDecimal getValorTotal() {
		BigDecimal soma = BigDecimal.ZERO;
		for (ItemOrcamento item : itens) {
			soma = soma.add(item.getValorTotal());
		}
		return soma;
	}
	
	public List<String> reservar() {
		LinkedList<String> erros = new LinkedList<String>();
		for (ItemOrcamento item : itens) {
			String erro = item.reservar();
			if (erro != null) {
				erros.add(erro);
			}
		}
		return erros;
	}
	
}
