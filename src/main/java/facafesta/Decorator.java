package facafesta;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

@WebFilter(
	urlPatterns="/*",
	dispatcherTypes={DispatcherType.FORWARD, DispatcherType.REQUEST})
public class Decorator implements Filter, Serializable {

	private static final long serialVersionUID = 1L;

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
		if (req instanceof HttpServletRequest) {
			HttpServletRequest httpreq = (HttpServletRequest) req;
			if (!httpreq.getRequestURI().matches(".+\\.(js|css|png|jpg)")) {
				PrintWriter out = resp.getWriter();
				out.append(Util.getText("facafesta/decorations/header.html"));
				mostrarMenu(out);
				try {
					out.append("<div class=\"corpo\">");
					chain.doFilter(req, resp);
					out.append("</div>");
				} catch (Throwable e) {
					Log.error(e);
					out.append("<div id=\"exception\"><div>"+e+"</div></div>");
				}
				List<String> erros = (List<String>) req.getAttribute("erros");
				mostrarErros(out, erros);
				out.append(Util.getText("facafesta/decorations/footer.html"));
				return;
			}
		}
		chain.doFilter(req, resp);
	}

	private void mostrarMenu(PrintWriter out) {
		EntityManager manager = Util.getEntityManager();
		List<String> categorias = manager.createQuery("select distinct categoria from Item").getResultList();
		out.append("<div class=\"menu\">");
		for (String categoria : categorias) {
			out.append("<span class=\"itemmenu\"><a href=\"/itens?categoria=");
			out.append(categoria);
			out.append("\">");
			out.append(categoria);
			out.append("</a></span>");
		}
		out.append("</div>");
		manager.close();
	}

	private void mostrarErros(PrintWriter out, List<String> erros) {
		if (erros != null && !erros.isEmpty()) {
			out.append("<div class=\"erros\" style=\"display:none;\">");
			for (String erro : erros) {
				out.append(erro);
			}
			out.append("</div>");
		}
	}

	@Override
	public void init(FilterConfig cfg) throws ServletException {
	}

}
