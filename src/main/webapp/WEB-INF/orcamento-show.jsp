<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h1>Or�amento</h1>
<c:if test="${orcamento eq null}">
	<span>O or�amento n�o cont�m nenhum item. Voc� pode consult�-los <a href="/itens">clicando aqui</a>.</span>
</c:if>
<c:if test="${orcamento ne null}">
	<table>
	  <tr>
	    <th>Descri��o</th>
	    <th>Valor Unit�rio</th>
	    <th>Quantidade</th>
	    <th>Valor Total</th>
	  </tr>
	  <c:forEach var="item" items="${orcamento.itens}">
	  <tr>
	    <td>${item.item.descricao}</td>
	    <td>${item.item.valor}</td>
	    <td>${item.quantidade}</td>
	    <td>${item.valorTotal}</td>
	  </tr>
	  </c:forEach>
	  <tr>
	  	<td colspan="2"><b>Total R$</b><td>
	  	<td><b>${orcamento.valorTotal}</b><td>
	  </tr>
	</table>

	<br/><br/><br/>
	<span class="opcao" ><a href="/reserva">Fazer Reserva</a></span>
	<span class="opcao"><a href="/email">Enviar para e-mail</a></span>
</c:if>
<br/><br/><br/>