<h1>Reserva</h1>
<form id="form" action="/reserva" method="post">
	<fieldset><legend>Evento</legend>
	<label for="data">Data:&nbsp;</label>
	<input type="text" id="data" name="data" class="data validate[required]" value="${reserva.data}" />
	<br>
	<label for="entrega">Entrega:&nbsp;</label>
	<input type="text" id="entrega" name="entrega" class="validate[required]" value="${reserva.entrega}" />
	<br>
	<label for="evento">Evento:&nbsp;</label>
	<input type="text" id="evento" name="evento" class="validate[required]" value="${reserva.evento}" />
	<br>
	<label for="endereco">Endere�o:&nbsp;</label>
	<input type="text" id="endereco" name="endereco" class="validate[required]" value="${reserva.endereco}" />
	<br>
	<label for="bairro">Bairro:&nbsp;</label>
	<input type="text" id="bairro" name="bairro" class="validate[required]" value="${reserva.bairro}" />
	<br>
	</fieldset>
	
	<fieldset><legend>Contato</legend>
	<label for="nome">Nome:&nbsp;</label>
	<input type="text" id="nome" name="nome" class="validate[required]" value="${reserva.nome}" />
	<br>
	<label for="email">E-mail:&nbsp;</label>
	<input type="text" id="email" name="email" class="email validate[required,custom[email]]" value="${reserva.email}" />
	<br>
	<label for="telefone">Telefone:&nbsp;</label>
	<input type="text" id="telefone" name="telefone" class="fone validate[required]" value="${reserva.telefone}" />
	<br>
	</fieldset>
	<input type="submit" class="submit" value="Efetuar Reserva" />
	<br/><br/>
</form>