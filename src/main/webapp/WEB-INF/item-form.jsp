<h1>Item</h1>
<form if="form" action="/item/novo" method="post">
	<fieldset><legend>Item</legend>
	<label for="categoria">Categoria:&nbsp;</label>
	<input type="text" id="categoria" name="categoria" class="validate[required]" value="${item.categoria}" />
	<br>
	<label for="descricao">Descri��o:&nbsp;</label>
	<input type="text" id="descricao" name="descricao" class="validate[required]" value="${item.descricao}" />
	<br>
	<label for="valor">Valor R$:&nbsp;</label>
	<input type="text" id="valor" name="valor" class="money validate[required]" value="${item.valor}" />
	<br>
	<label for="quantidade">Quantidade dispon�vel:&nbsp;</label>
	<input type="text" id="quantidade" name="quantidade" class="integer validate[required]" value="${item.quantidade}" />
	<br>
	</fieldset>
	<input type="submit" class="submit" value="Salvar Item" />
	<br/><br/>
</form>
