<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h1>Itens</h1>
<div class="itens">
<c:forEach var="item" items="${itens}">
	<div class="item">
		<label>Categoria: </label>
		<span>${item.categoria}</span>
		<br>
		<label>Descri��o: </label>
		<span>${item.descricao}</span>
		<br>
		<label>Valor: R$</label>
		<span>${item.valor}</span>
		<br>
		<a href="/item/${item.id}">Detalhar</a>
	</div>
</c:forEach>
</div>
