<div>
	<h1>Item</h1>
	<label>Categoria: </label>
	<span>${item.categoria}</span>
	<br>
	<label>Descri��o: </label>
	<span>${item.descricao}</span>
	<br>
	<label>Valor: R$</label>
	<span>${item.valor}</span>
	<br>
	<label>Quantidade dispon�vel: </label>
	<span>${item.quantidade}</span>
	<br>
</div>
<div>
	<h1>Or�amento</h1>
	<form id="form" action="/orcamento/adicionaritem" method="post">
		<input type="hidden" name="item" value="${item.id}" />
		<label for="quantidade">Quantidade a or�ar: </label>
		<input type="text" name="quantidade" id="quantidade" class="integer validate[required]" />
		<input type="submit" value="Adicionar ao Or�amento" />
	</form>
</div>