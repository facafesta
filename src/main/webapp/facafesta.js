function ready() {
	$(".erros").children(".field").each(function (index, obj) {
		var id = "#" + $(this).text();
		$(id).after("<span class=\"field-error\">Valor inv�lido.</span>");
	});
	$(".money").maskMoney({decimal:",", thousands:"."});
	$(".integer").numeric();
	$(".data").mask("99/99/9999");
	$(".fone").mask("(99) 9999-9999");
	$("#form").validationEngine();
}